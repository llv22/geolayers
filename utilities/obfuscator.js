const JavaScriptObfuscator = require('javascript-obfuscator');
const fs = require('fs');
const obfuscate = (fileName) => {
  fs.readFile(fileName,'utf8', function(err, data) {
    if (err) {console.log(err)};
    let obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    let uglyCode = obfuscationResult.getObfuscatedCode();
    outFileName = fileName.replace('.js', '.ugly.js');
    fs.writeFile(outFileName, uglyCode, function (err) {
        if (err) throw err;
        console.log(`${fileName} has been obfuscated at ${outFileName}`);
      });
    })
};
module.exports = { obfuscate };