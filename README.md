<!-- markdownlint-disable MD029 -->
# Geological layers

geological layers with location diagnosis and video checking-up

## Getting started

1. register your user in <https://gitlab.com/dashboard/projects>
2. sync your code to local

```bash
git clone https://gitlab.com/llv22/geolayers.git
```

About git operations, refer to [git introduction in Chinese](https://blog.csdn.net/Lakers2015/article/details/114659213).

3. start on macOS

```bash
"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" --allow-file-access-from-files
```

4. confuser in <https://tool.bfw.wiki/tool/1559286383549195.html>

5. refactoring based on map hotspot generator

* copy templates/static_image_host.html to the root folder of project
* use Dreamweaver to define hotspot, setup navigation paths

6. javascript obfuscator

* install node.js
* enter scripts to install npm dependencies

```bash
npm install
npm audit fix --force
node command obf ../resources/scripts/maputil-1.0.0.js
```

* obfuscate *.html via toolkit <https://tool.bfw.wiki/tool/1559286383549195.html>
    1. obfuscate vpopup.html
    2. obfuscate other html pages, change link in resources/scripts/maputil-1.0.0.js to resources/scripts/maputil-1.0.0.ugly.js, change vpopup.html to the obfuscated version of vpopup.html

## License

For open source projects, say how it is licensed.

## Project status

This project is still under investigation and codes will be adjusted time by time, which majorly serves the purpose of technical check-up. Therefore, there shouldn't be any geological-specific scenario involved in code. This is also the general guideline of our current project.
