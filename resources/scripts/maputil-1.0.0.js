var windowObjectReference = null;
var PreviousUrl;
var vWindowObjectReference = null;
var vPreviousUrl;

function estimateSize(rWidth, rHeight) {
    const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
    const left = 0;
    const top = 0;
    return [rWidth, rHeight, width, height, left, top];
}

function popup(url, hostPage=null) {
    if(url.endsWith("mov")||url.endsWith("mp4")){
        if (hostPage == null) {
            throw 'must specify host page';
        }
        var newVideo = document.createElement("video");
        newVideo.src = url;
        newVideo.addEventListener("loadedmetadata", function videoLoad() {
            this.removeEventListener("loadedmetadata", videoLoad);
            const s = estimateSize(newVideo.videoWidth, newVideo.videoHeight);
            const rWidth = s[0], rHeight = s[1], width = s[2], height = s[3], left = s[4], top = s[5];
            var vWidth = Math.min(rWidth/4, width/4), vHeight = Math.min(rHeight/4, height/4);
            if (vWidth < 700){
                vWidth = 700;
            }
            if (vHeight < 500){
                vHeight = 500;
            }

            if (windowObjectReference != null) {
            windowObjectReference.close()
            }

            if(vWindowObjectReference == null || vWindowObjectReference.closed) {
                vWindowObjectReference = window.open(hostPage+"?resource="+url+"&vWidth="+vWidth+"&vHeight="+vHeight, "",
                "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,resizable=no,scrollbars=no,width="+vWidth+",height="+vHeight+",left="+left+",top="+top);
            } else if(vPreviousUrl != url) {
                vWindowObjectReference = window.open(hostPage+"?resource="+url, "",
                "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,resizable=no,scrollbars=no,width="+vWidth+",height="+vHeight+",left="+left+",top="+top);
            vWindowObjectReference.focus();
            } else {
                vWindowObjectReference.focus();
            };
            vPreviousUrl = url;
        }, false);
    }
    else if(url.endsWith("jpg")){
        var newImg = document.createElement("img");
        newImg.src = url;
        newImg.addEventListener("load", function imgLoad() {
            this.removeEventListener("load", imgLoad);
            const s = estimateSize(newImg.width, newImg.height);
            const rWidth = s[0], rHeight = s[1], left = s[4], top = s[5];

            if (vWindowObjectReference != null) {
                vWindowObjectReference.close()
            }

            if(windowObjectReference == null || windowObjectReference.closed) {
                windowObjectReference = window.open(url, "", 
                    "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,resizable=no,scrollbars=no,width="+rWidth+",height="+rHeight+",left="+left+",top="+top);
            } else if(PreviousUrl != url) {
                windowObjectReference = window.open(url, "",
                    "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,resizable=no,scrollbars=no,width="+rWidth+",height="+rHeight+",left="+left+",top="+top);
                windowObjectReference.focus();
            } else {
                windowObjectReference.focus();
            };
            PreviousUrl = url;
        }, false);
    }
    else {
        if (vWindowObjectReference != null) {
            vWindowObjectReference.close()
            }
        if(windowObjectReference == null || windowObjectReference.closed) {
            windowObjectReference = window.open(url, "", 
                "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,resizable=no,scrollbars=no");
            } 
        else if(PreviousUrl != url) {
            windowObjectReference = window.open(url, "",
            "directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,resizable=no,scrollbars=no");
            windowObjectReference.focus();
        } else {
            windowObjectReference.focus();
        };
        PreviousUrl = url;
    }
}

function formatDigital(object) {
    return object.toLocaleString('en-US', {minimumIntegerDigits: 1, maximumFractionDigits: 1, useGrouping:false});
}

function imgLoad() {
    var img = document.getElementById('geoImg');
    window.resizeTo(img.width + 10, img.height + 70);
}